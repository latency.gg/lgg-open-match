package lgg

import (
	"errors"
	"time"

	"github.com/golang/protobuf/ptypes"
	"open-match.dev/open-match/pkg/pb"
)

const (
	lggResultKey = "lggValues"
	ipAddressKey = "ipAddress"
	locationsKey = "locations"
)

type Credentials struct {
	BaseUrl string
	ApiKey  string
	Timeout time.Duration
}

type LGG struct {
	metricsProvider MetricsProvider
}

func NewLGG(credentials Credentials) *LGG {
	return NewLGGWithProvider(
		NewHttpMetricsProvider(
			credentials.BaseUrl,
			credentials.ApiKey,
			credentials.Timeout,
		),
	)
}

func NewLGGWithProvider(provider MetricsProvider) *LGG {
	return &LGG{
		metricsProvider: provider,
	}
}

func (lgg *LGG) SearchBestLatencies(provider string, matches []*pb.Match) []error {
	var errorList []error
	if lgg.metricsProvider == nil {
		errorList = append(errorList, errors.New("no latency metrics provider given"))
		return errorList
	}

	for _, match := range matches {
		if match.Tickets == nil || len(match.Tickets) == 0 {
			continue
		}

		for _, ticket := range match.Tickets {
			if ticket.Extensions == nil || len(ticket.Extensions) == 0 {
				continue
			}

			ipAddressExt := ticket.Extensions[ipAddressKey]
			locationsExt := ticket.Extensions[locationsKey]
			if ipAddressExt == nil || locationsExt == nil {
				// this ticket has no interest in the best latency
				continue
			}

			ipAddress := &IPAddress{}
			locations := &LocationSet{}

			err := ptypes.UnmarshalAny(ipAddressExt, ipAddress)
			if err != nil {
				errorList = append(errorList, err)
			}

			err = ptypes.UnmarshalAny(locationsExt, locations)
			if err != nil {
				errorList = append(errorList, err)
			}

			if err != nil {
				continue
			}

			var latencyEntries []*LatencyEntry

			for _, location := range locations.Locations {
				res, err := lgg.metricsProvider.Get(provider, location, ipAddress.Value)
				if err != nil {
					errorList = append(errorList, err)
					continue
				}

				latencyEntries = append(latencyEntries, &LatencyEntry{
					Location: location,
					Latency:  int32(res.RTT),
				})
			}

			latencySet, err := ptypes.MarshalAny(&LatencySet{
				Entries: latencyEntries,
			})

			if err != nil {
				errorList = append(errorList, err)
				continue
			}

			ticket.Extensions[lggResultKey] = latencySet
		}
	}

	return errorList
}
