package lgg

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"time"

	l "gitlab.com/latency.gg/lgg-open-match/internal/pkg/lgg"
)

type Provider struct {
	Name      string   `json:"name"`
	Locations []string `json:"locations"`
}

type LocationProvider interface {
	Get(provider string) (*Provider, error)
	GetAll() ([]Provider, error)
}

type DummyLocationProvider struct {
	providers []Provider
}

type HttpLocationProvider struct {
	client  *http.Client
	BaseUrl string
	ApiKey  string
}

func NewHttpLocationProvider(
	baseUrl, apiKey string,
	timeout time.Duration,
) LocationProvider {
	return &HttpLocationProvider{
		client: &http.Client{
			Timeout: timeout,
		},

		BaseUrl: baseUrl,
		ApiKey:  apiKey,
	}
}

func NewDummyLocationProvider(locations []Provider) LocationProvider {
	return &DummyLocationProvider{locations}
}

func (p *HttpLocationProvider) Get(providerName string) (*Provider, error) {
	u := url.URL{
		Scheme: "https",
		Host:   p.BaseUrl,
		Path:   fmt.Sprintf("provider/%v", providerName),
	}

	bytes, err := l.HttpGetWithBearerToken(p.client, u, p.ApiKey)
	if err != nil {
		return nil, err
	}

	provider := &Provider{}

	err = json.Unmarshal(bytes, provider)
	if err != nil {
		return nil, err
	}

	return provider, nil
}

func (p *HttpLocationProvider) GetAll() ([]Provider, error) {
	u := url.URL{
		Scheme: "https",
		Host:   p.BaseUrl,
		Path:   "provider",
	}

	bytes, err := l.HttpGetWithBearerToken(p.client, u, p.ApiKey)
	if err != nil {
		return nil, err
	}

	var providers []Provider

	err = json.Unmarshal(bytes, &providers)
	if err != nil {
		return nil, err
	}

	return providers, nil
}

func (p *DummyLocationProvider) Get(provider string) (*Provider, error) {
	for _, prov := range p.providers {
		if prov.Name == provider {
			return &prov, nil
		}
	}

	return nil, nil
}

func (p *DummyLocationProvider) GetAll() ([]Provider, error) {
	return p.providers, nil
}
