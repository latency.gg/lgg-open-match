package lgg

import (
	"testing"

	"github.com/golang/protobuf/ptypes"
	any "github.com/golang/protobuf/ptypes/any"
	"open-match.dev/open-match/pkg/pb"
)

func TestLGG(t *testing.T) {
	var matches []*pb.Match

	extensions := make(map[string]*any.Any)

	ipAddr, err := ptypes.MarshalAny(&IPAddress{
		Value: "35.28.21.4",
	})
	if err != nil {
		t.Error(err)
	}

	locs, err := ptypes.MarshalAny(&LocationSet{
		Locations: []string{"eu-central-1"},
	})
	if err != nil {
		t.Error(err)
	}

	extensions["ipAddress"] = ipAddr
	extensions["locations"] = locs

	var tickets []*pb.Ticket

	tickets = append(tickets, &pb.Ticket{
		Extensions: extensions,
	})

	matches = append(matches, &pb.Match{
		Tickets: tickets,
	})

	lgg := NewLGGWithProvider(NewDummyMetricsProvider(Metrics{
		RTT: 20,
	}))

	errs := lgg.SearchBestLatencies("aws", matches)
	for _, err := range errs {
		t.Error(err)
	}

	// TODO finish test with a dummy provider
}
